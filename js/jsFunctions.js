var mudaFtInterv=""; //nesta variável será iniciado o intervalo
var numAtual = 0;


//Função criada para carregar as imagens automaticamente trocando-as em um intervalo de tempo definido em 5 segundos =	5000 milissegundos
function onLoadImg(){		
    document.getElementById("num").style.display = "none";
    document.getElementById("carregando").style.border="none";
    document.getElementById("num").innerHTML = numAtual;
	if (numAtual >= 0 && numAtual <= 5) {
            mudaFtInterv = setTimeout("mostraFoto(numAtual)", 5000);
            numAtual++;
        };
}

//Função criada para alternar as imagens que estão no repositório. São 5 imagens em princípio, para inserir mais imagens, ajustar aqui.
function mostraFoto(num){
        if (mudaFtInterv!==""){
            document.getElementById("carregando").style.display="none";
            clearTimeout(mudaFtInterv);
        };

        if (num <= 0 || num >= 5) {
            numAtual = 1;
        } else{
            numAtual = num;
        };
        
        document.getElementById("foto").style.display="";
        var ft = document.getElementById("foto");
        ft.alt = "Imagem "+numAtual+"/"+5;
        ft.src = "img/slide/" + numAtual + ".jpg";
}

/* Array de objetos para listar em uma DIV específica*/
//criando array
var alunos = new Array();
//Contador que ajudará a posicionar e pegar elementos pela posição

//Construtor do Objeto que será inserido no array
function Aluno(id, nome, telefone, cpf, curso, email){
    this.ID = id;
    this.Nome = nome;
	this.Telefone = telefone;
    this.CPF = cpf;
    this.Curso = curso;
    this.Email = email;
}

//Uma mensagem de erro deve aparecer SEMPRE que o usuário deixar o campo vazio ou se o primeiro caractere não for uma letra (de "A" a "Z" ou de "a" a "z").
function validaNome(nome){
    regex = /\b[A-Za-zÀ-ú][A-Za-zÀ-ú]+,?\s[A-Za-zÀ-ú][A-Za-zÀ-ú]{1,12}\b/gi;

    if (regex.test(nome.value)) {
		nome.select();
		nome.setAttribute("class","textbox");
		msgReturn("Nome Validado",2);
        return true;
    } else {
        nome.select();
        nome.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha o seu nome, o campo não pode ser vazio e o primeiro caractere precisa ser uma letra de A a Z ou de a a z. Ex.: Fulano de Tal",1);
		return false;
		window.onload();
    }
}

//Uma mensagem de erro deve aparecer sempre que o campo estiver vazio ou contiver qualquer caractere que não seja um dígito de 0 a 9.
function validaTelefone(telefone){
	regex =  /^\d{11}$/;
	///^\(?(\d{3})\)?[\.\-\/]?(\d{4})[\.\-\/]?(\d{4})$/;
	
    if (regex.test(telefone.value)) {
		telefone.select();
		telefone.setAttribute("class","textbox");
		msgReturn("Telefone Validado",2);
        return true;
    } else {
        telefone.select();
        telefone.setAttribute("class","textboxError");
        msgReturn("Cadastre o telefone corretamente! Não pode estar vazio ou conter qualquer caractere que não seja um dígito de 0 a 9. Ex.: (000)0000-0000",1);
		return false;
		window.onload();
    }
}

//Validando o email digitado no input de email, consistindo de tem "@" e se tem ".com" ou similar
function validarEmail(email){
    regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (regex.test(email.value)) {
		email.select();
		email.setAttribute("class","textbox");
		msgReturn("Email Validado",2);
        return true;
    } else {
        email.select();
        email.setAttribute("class","textboxError");
        msgReturn("Por favor, preencha um e-mail valido. Ex.: nome@provedor.com",1);
    return false;
    }
}

//Para validar o formato do CPF digitado no input
function validaCPF(cpf){
    regex = /^[\d]{3}\.[\d]{3}\.[\d]{3}\-[\d]{2}$/;

    if (regex.test(cpf.value)) {
		cpf.select();
		cpf.setAttribute("class","textbox");
		msgReturn("CPF Validado",2);
        return true;
    } else {
        cpf.select();
		cpf.setAttribute("class","textboxError");
        msgReturn("CPF Invalido. Ex.: 000.000.000-00.",1);
    return false;
    }
}

//Aqui eu tenho um case para ver qual mensagem vou retornar para o usuário. Ainda tem um temporizador para fechar a mensagem.
function msgReturn(msg, tipo){
    switch (tipo) {
        case 1:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-sucess");
            $('#returnMsg').addClass("alert alert-danger");
        break;
        case 2:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').addClass("alert alert-warning");
        break;
        case 3:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').addClass("alert alert-sucess");
        break;
        default:
            $('#returnMsg').removeClass("alert alert-danger");
            $('#returnMsg').removeClass("alert alert-warning");
            $('#returnMsg').removeClass("alert alert-success");
            $('#returnMsg').addClass("alert alert-success");
        break;
    }

    $("#returnMsg").fadeTo(1, 1).removeClass('hidden');
    $("#returnMsg").show();
    $("#msgReturn").show();

    if (msg !== "") {
        document.getElementById("msgReturn").innerHTML = msg;
    }

    window.setTimeout(function() {
    $("#returnMsg").fadeTo(1000, 0).slideUp(1000, function(){
        $("#returnMsg").addClass('hidden');
    });
    }, 1000);
}


//Validações sempre pegando os valores pelo ID da tags
//Para facilitar, todas as checagens devem ser realizadas APÓS o usuário da aplicação clicar em um botão cujo rótulo é "submeter" (Item D).
function validar(){
var nome = document.getElementById("nome");
var telefone = document.getElementById("telefone");
var cpf = document.getElementById("cpf");
var curso = document.getElementById("curso");
var email = document.getElementById("email");
var sexo = null;

//Validação geral se um dos elementos está vazio já devolve mensagem avisando para terminar o preenchimento
if ((nome.value === "")&&(telefone.value === "")&&(cpf.value === "")&&(curso.value === "")&&(email.value === "")) {
        nome.setAttribute("class","textboxError");
        telefone.setAttribute("class","textboxError");
        cpf.setAttribute("class","textboxError");
		curso.setAttribute("class","textboxError");
        email.setAttribute("class","textboxError");
        nome.select();
        msgReturn("Preencha o Formulário Corretamente!",2);
} else {
    if (validaNome(nome)){
        nome.setAttribute("class","textboxOK");
			if (validaTelefone(telefone)) {
				telefone.setAttribute("class","textboxOK");
				if (validaCPF(cpf)) {
					cpf.setAttribute("class","textboxOK");
					if (validarEmail(email)){
						nome.setAttribute("class","textboxOK");
						telefone.setAttribute("class","textboxOK");
						cpf.setAttribute("class","textboxOK");
						curso.setAttribute("class","textboxOK");
						email.setAttribute("class","textboxOK");
						msgReturn("INSERIDO!",2);

						//cria um objeto e cadastra no array
						montaTabela(nome, telefone, cpf, curso, email);
						limpaForm();
					}
				}
			}
		}
	}
}

//classe responsável por montar a tabela com os elementos dos OBJETOS do Array
function montaTabela(nome, telefone, cpf, curso, email){
	//caso tenha id é para editar o array, se estiver vazio add
	if (document.getElementById("idUser").value !== "") {
		var idUser = document.getElementById("idUser").value;
		alunos[idUser].ID = idUser;
		alunos[idUser].Nome = nome.value;
		alunos[idUser].Telefone = telefone.value;
		alunos[idUser].CPF = cpf.value;
		alunos[idUser].Curso = curso;
		alunos[idUser].Email = email.value;
		document.getElementById("idUser").value = "";
	} else {
		var cont = alunos.length;
		aluno = new Aluno(cont++,nome.value,telefone.value,cpf.value,curso.value,email.value);
    	alunos.push(aluno); //INSERIDO OBJETO ALUNO NO ARRAY ALUNOS
	}

    var sb = new StringBuilder();

    sb.append('<table class="table table-striped">');
    sb.append('<thead>');
    sb.append('<tr>');
    sb.append('<th>Print</th>');
    sb.append('<th>Nome</th>');
	sb.append('<th>Telefone</th>');
    sb.append('<th>CPF</th>');
    sb.append('<th>Curso</th>');
    sb.append('<th>E-mail</th>');
    sb.append('<th>Editar</th>');
    sb.append('<th>Excluir</th>');
    sb.append('</tr>');
    sb.append('</thead>');
    sb.append('<tbody>');
	//laço para posiciona-los por linha e coluna
    for (var i=0; i<alunos.length; i++) {
        sb.append('<tr>');
        sb.append('<td><a href="javascript:abrirJanela('+alunos[i].ID+');"><img src="img/24px.svg" style="max-width: none !important; width: 30px; margin-left: 0px;" /></a></td>');
        sb.append('<td id="nome">'+alunos[i].Nome+'</td>');
		sb.append('<td id="telefone">'+alunos[i].Telefone+'</td>');
        sb.append('<td id="cpf" style="width:85px;">'+alunos[i].CPF+'</td>');
        sb.append('<td id="curso" style="text-align: center;">'+alunos[i].Curso+'</td>');
        sb.append('<td id="email">'+alunos[i].Email+'</td>');
        sb.append('<td><a href="javascript:editarAluno('+alunos[i].ID+');"><img src="img/editar.jpg" style="max-width: none !important; width: 23px; margin-left: 5px;" /></a></td>');
        sb.append('<td><a href="javascript:excluirAluno('+alunos[i].ID+');"><img src="img/excluir.jpg" style="max-width: none !important; width: 30px; margin-left: 3px;" /></a></td>');
        sb.append('</tr>');
    }
    sb.append('</tbody>');
    sb.append('</table>');
    
    document.getElementById("tabela").innerHTML = sb.toString();
}

//limpando o form para novo cadastro
function limpaForm(){    
    $('#nome').removeClass("textboxOK");
    $('#nome').addClass("textbox");
    document.getElementById('nome').value = "";
	$('#telefone').removeClass("textboxOK");
    $('#telefone').addClass("textbox");
    document.getElementById('telefone').value = "";
    $('#cpf').removeClass("textboxOK");
    $('#cpf').addClass("textbox");
    document.getElementById('cpf').value = "";
    $('#email').addClass("textbox");
    document.getElementById('email').value = "";
}

function limpaFormNome(){    
    $('#nome').removeClass("textboxOK");
    $('#nome').addClass("textbox");
    document.getElementById('nome').value = "";
}

function StringBuilder(value) {
    this.strings = new Array();
    this.append(value);
}

StringBuilder.prototype.append = function (value) {
    if (value) {
        this.strings.push(value);
    }
}

StringBuilder.prototype.clear = function () {
    this.strings.length = 0;
}

StringBuilder.prototype.toString = function () {
    return this.strings.join("");
}

//Funcao para excluir aluno
function excluirAluno(id){
	//REMOVENDO DO ARRAY PELO ID
	alunos.splice(id, 1);

	var sb = new StringBuilder();
	//LIMPANDO OS ELEMENTOS HTML DA TABELA 
	document.getElementById("tabela").innerHTML = "";
	
    sb.append('<table class="table table-striped">');
    sb.append('<thead>');
    sb.append('<tr>');
    sb.append('<th>Print</th>');
    sb.append('<th>Nome</th>');
	sb.append('<th>Telefone</th>');
    sb.append('<th>CPF</th>');
    sb.append('<th>Curso</th>');
    sb.append('<th>E-mail</th>');
    sb.append('<th>Editar</th>');
    sb.append('<th>Excluir</th>');
    sb.append('</tr>');
    sb.append('</thead>');
    sb.append('<tbody>');
	//laço para posiciona-los por linha e coluna
    for (var i=0; i<alunos.length; i++) {
        sb.append('<tr>');
        sb.append('<td><a href="javascript:abrirJanela('+(i)+');"><img src="img/24px.svg" style="max-width: none !important; width: 30px; margin-left: 0px;" /></a></td>');
        sb.append('<td id="nome">'+alunos[i].Nome+'</td>');
		sb.append('<td id="telefone">'+alunos[i].Telefone+'</td>');
        sb.append('<td id="cpf" style="width:85px;">'+alunos[i].CPF+'</td>');
        sb.append('<td id="curso" style="text-align: center;">'+alunos[i].Curso+'</td>');
        sb.append('<td id="email">'+alunos[i].Email+'</td>');
        sb.append('<td><a href="javascript:editarAluno('+(i)+');"><img src="img/editar.jpg" style="max-width: none !important; width: 23px; margin-left: 5px;" /></a></td>');
        sb.append('<td><a href="javascript:excluirAluno('+(i)+');"><img src="img/excluir.jpg" style="max-width: none !important; width: 30px; margin-left: 3px;" /></a></td>');
        sb.append('</tr>');
    }
    sb.append('</tbody>');
    sb.append('</table>');
    
    document.getElementById("tabela").innerHTML = sb.toString();
}

//Função para editar o elemento selecionado <a href editarAluno()>
function editarAluno(id){
	
	document.getElementById("idUser").value = id;
	document.getElementById("nome").value = alunos[id].Nome;
	document.getElementById("telefone").value = alunos[id].Telefone;
	document.getElementById("cpf").value = alunos[id].CPF;
	document.getElementById("curso").value = alunos[id].Curso;
	document.getElementById("email").value = alunos[id].Email;
}


//Função para imprimir em pdf com o elemento selecionado no clique do mouse <a href abrirJanela()>
function abrirJanela(id){ 
	var novonome = alunos[id].Nome;
	var novotelefone = alunos[id].Telefone;
	var novocpf = alunos[id].CPF;
	var novocurso = alunos[id].Curso;
	var novoemail = alunos[id].Email;

	var style = "<style>";
	style = style + "table {width: 100%;font: 20px Calibri;}";
	style = style + "table, th, td {border: solid 1px #DDD; border-collapse: collapse;";
	style = style + "padding: 2px 3px;text-align: center;}";
	style = style + "</style>";
	// CRIA UM OBJETO WINDOW
	var win = window.open('', '', 'height=1000,width=1000');
	win.document.write('<!DOCTYPE html><html lang="pt-br"><meta charset="UTF-8"><title>Cadastro</title><link rel="stylesheet" href="css/style.css" type="text/css" media="all"><link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all"><script type="text/javascript" src="js/jquery.min.js"></script><script type="text/javascript" src="js/bootstrap.min.js"></script><script type="text/javascript" src="js/jquery.mask.js"></script>      <script type="text/javascript" src="js/jsFunctions.js" charset="UTF-8"></script>');
	win.document.write(style);                    	// INCLUI UM ESTILO NA TAB HEAD
	win.document.write('</head>');
	win.document.write('<body><header><div class="container"><h1>Atividade de Integração I</h1></div>');
	win.document.write('<img src="../Tarefa 7/img/furg.jpg" class="top"/>');
	win.document.write('<form id="form1"><div class="rowLabel"><label>Nome: </label>');   // O CONTEUDO DA TABELA DENTRO DA TAG BODY
	win.document.write(novonome);
	win.document.write('</div><div class="rowLabel"><label>Telefone: </label>');   // O CONTEUDO DA TABELA DENTRO DA TAG BODY
	win.document.write(novotelefone);
	win.document.write('</div><div class="rowLabel"><label>CPF: </label>');
	win.document.write(novocpf);
	win.document.write('</div><div class="rowLabel"><label>Curso: </label>');
	win.document.write(novocurso);
	win.document.write('</div><div class="rowLabel"><label>Email: </label>');
	win.document.write(novoemail);
	win.document.write('</div>');
	win.document.write('</form><footer><p class="container">Atividade de Integração I</p><address>Douglas Ristow <br/>Thiarles Cunha</address></footer></body></html>');
	win.document.close(); 	                       // FECHA A JANELA
	win.print();                                   // IMPRIME O CONTEUDO
}

